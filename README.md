# exploratory-data-analysis-lending-club

In this project, we will use R programming language and the application of exploratory data analysis techniques to verify relationships in one or more variables and to explore a specific data set to find distributions, outliers, and anomalies.